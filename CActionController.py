import subprocess
import threading

import CSpeechModule
import CInputController
import CMusicController
import CTextUtils
import CLMStudioServerModule

# Global variables
listening: bool = True
typingModeEngaged: bool = False

announcedListen: bool = False
announcedTypingMode: bool = False

LastText: str = ""

MusicControlPhrases: list = [
    "stop",
    "pause",
    "play",
    "resume",
    "continue"
]
AskLocalAIModelActivationPhrases: list = [
    "tell me what",
    "tell me about",
    "tell me if",
    "tell me who"
]

def DoActions(isListening: bool = False, SpokenText: str = "undefined", OriginalText: str = "Undefined"):
    global typingModeEngaged, announcedTypingMode, LastText

    if SpokenText != LastText:
        LastText = SpokenText
    else:
        return

    if not isListening:
        return;

    if typingModeEngaged:
        try:
            CInputController.TypeDictation(OriginalText)
        except Exception as e:
            print(f"Error calling CInputController.SimulateTypingSequence: {e}")
    else:
        typingModeEngaged = False

    if SpokenText.replace(".", "").replace("?", "") == "jarvis":
        CSpeechModule.SpeakWithVolume("Yes?", 25)

    if "enter typing mode" in SpokenText:
        CSpeechModule.SpeakWithVolume("Typing mode engaged.", 25)
        typingModeEngaged = True
        announcedTypingMode = False

    if "exit typing mode" in SpokenText:
        CSpeechModule.SpeakWithVolume("Typing mode disengaged.", 25)
        typingModeEngaged = False
        announcedTypingMode = False

    if "switch to workspace" in SpokenText or "switched to workspace" in SpokenText:
        try:
            # Extract the workspace number from the command
            SpokenText = SpokenText.replace(".", "")
            SpokenText = SpokenText.replace("...", "")
            _, _, workspace_number = SpokenText.partition("workspace ")
            workspace_number = CTextUtils.ExtractNumberFromText(workspace_number)
            CInputController.SimulateKeyPress(f"Super_L+{workspace_number}")
        except Exception as e:
            print(f"Error processing command: {e}")

    if "start app" in SpokenText:
        # Extract the program name
        try:
            command_parts = SpokenText.split("start app", 1)

            if len(command_parts) > 1:
                program_name = command_parts[1].strip()  # Program name after 'start'
                start_program(program_name)
        except Exception as e:
            print(f"Error processing command: {e}")

    if "boot" in SpokenText:
        # Extract the program name
        try:
            command_parts = SpokenText.split("boot", 1)

            if len(command_parts) > 1:
                vm_name = command_parts[1].strip()  # Program name after 'start'
                start_vm(vm_name)
        except Exception as e:
            print(f"Error processing command: {e}")

    if "shutdown" in SpokenText:
        # Extract the program name
        try:
            command_parts = SpokenText.split("shutdown", 1)

            if len(command_parts) > 1:
                vm_name = command_parts[1].strip()  # Program name after 'start'
                gracefully_shutdown_vm(vm_name)
        except Exception as e:
            print(f"Error processing command: {e}")

    if "date" in SpokenText:
        try:
            # Execute the 'date' command with formatted output
            format_str = "+%A %d %B %I:%M %p"
            result = subprocess.run(["date", format_str], capture_output=True, text=True)
            weekday, day, month, time, am_pm = result.stdout.strip().split(" ")

            # Strip leading zero from the day and add correct ordinal suffix
            day = day.lstrip("0")
            ordinal_suffix = CTextUtils.GetOrdinalSuffix(int(day))
            day_with_suffix = f"{day}{ordinal_suffix}"

            # Strip leading zero from the time
            time = time.lstrip("0")

            date_time_output = f"{weekday}, {day_with_suffix} {month}, {time} {am_pm}"

            # Use espeak to dictate the formatted date and time output
            CSpeechModule.SpeakWithVolume(date_time_output, 25)
            print(f"Dictated: {date_time_output}")
        except Exception as e:
            print(f"Error dictating date: {e}")

    if any(phrase in SpokenText for phrase in AskLocalAIModelActivationPhrases):
        # CLMStudioServerModule.AskLocalAIModel(SpokenText) # Blocks the main thread for a good while.
        LocalAIModelThread = threading.Thread(target=CLMStudioServerModule.AskLocalAIModel, args=(SpokenText,))
        LocalAIModelThread.start()

    # Used to kill the espeak process that spawns when a response from LM Studio is being spoken
    if "jarvis stop" in SpokenText or "javas stop" in SpokenText:
        subprocess.Popen(["/usr/bin/pkill", "espeak"])
        CSpeechModule.SpeakWithVolume("Stopped.")
        return

    if any(phrase in SpokenText for phrase in MusicControlPhrases):
        CMusicController.SimulatePlayPause()


def start_program(program_name):
    # Remove punctuation or commas from the program name since Whisper captures that quite well.
    program_name = CTextUtils.CleanText(program_name)

    try:
        # Adjust the path or command format as needed
        subprocess.Popen([f"/usr/bin/{program_name}"])
        CSpeechModule.SpeakWithVolume(f"Starting {program_name}")
        print(f"Started program: {program_name}")
    except Exception as e:
        CSpeechModule.SpeakWithVolume(f"Error starting program: {e}")
        print(f"Error starting program: {e}")


def start_vm(vm_name):
    # Remove punctuation or commas from the program name since Whisper captures that quite well.
    vm_name = CTextUtils.CleanText(vm_name)
    vm_name = CTextUtils.FormatVMName(vm_name)

    if vm_name == "shows":
        try:
            subprocess.Popen(["/usr/bin/vmrun", "-T", "player", "start", "/mnt/1-TB-HDD/VMWare/Manjaro/Manjaro.vmx"])
            print("Started VMWare Shows VM")
        except Exception as e:
            print(f"Error starting VM: {e}")

        return;

    try:
        # Adjust the path or command format as needed
        subprocess.Popen(["/usr/bin/sudo", "/usr/bin/virsh", "start", vm_name])
        print(f"Started VM: {vm_name}")
    except Exception as e:
        print(f"Error starting VM: {e}")

def gracefully_shutdown_vm(vm_name):
    # Remove punctuation or commas from the program name since Whisper captures that quite well.
    vm_name = CTextUtils.CleanText(vm_name)
    vm_name = CTextUtils.FormatVMName(vm_name)

    try:
        # Adjust the path or command format as needed
        subprocess.Popen(["/usr/bin/sudo", "/usr/bin/virsh", "shutdown", vm_name])
        print(f"Shutting down VM: {vm_name}")
    except Exception as e:
        print(f"Error shutting down VM: {e}")
