import subprocess
import re


"""
Set up a list of phrases which aren't allowed to be typed out. First four are the toggle commands for the dictation mode, while
"you" is a phrase that comes out when Whisper doesn't detect any input for some reason.
"""
DisallowedPhrases: list = [
    "Enter typing mode.",
    "Exit typing mode.",
    "Exit typing mode",
    "exit typing mode",
    "you"
]


def SimulateKeyPress(key: str = ""):
    try:
        subprocess.run(["xdotool", "key", key])
        print(f"Keypress of key {key} sent.")
    except Exception as e:
        print(f"Error sending keypress {key}: {e}")


def SimulateTypingSequence(text: str = "Undefined"):
    if text == "space.":
        SimulateKeyPress("space")
        return

    if text == "backspace.":
        SimulateKeyPress("0xff08")
        return
    
    try:
        # noinspection PyTypeChecker
        subprocess.run(["xdotool", "type", "--delay", "100", text])
        print(f"Trying to simulate typing sequence of sentence {text}")
    except Exception as e:
        print(f"Error simulating typing sequence of sentence {text}: {e}")


def TypeDictation(text: str = "Undefined"):
    global DisallowedPhrases

    if any(phrase in text for phrase in DisallowedPhrases):
        return

    if re.search(r'[^a-zA-Z0-9\s!.,;:\'\"@#$%^&*()_+=-\?]', text):
        return

    SimulateTypingSequence(text)
