import openai

import CSpeechModule

openai.api_base = "http://localhost:1234/v1"
openai.api_key = ""

def AskLocalAIModel(request: str = "Undefined"):
    CSpeechModule.SpeakWithVolume("Working on it... This could take some time.")

    try:
        completion = openai.ChatCompletion.create(
            model="local-model",
            messages=[
                {"role": "system", "content": "Always answer the user directly."},
                {"role": "user", "content": request}
            ]
        )

        CSpeechModule.SpeakWithVolume(completion.choices[0].message.content)
    except openai.error.APIConnectionError as _:
        CSpeechModule.SpeakWithVolume("Your request cannot be completed, as the connection to LM Studio cannot be established.")
    except openai.error.ServiceUnavailableError as _:
        print("LM Studio is currently generating. Can happen because of too long pauses inbetween sentences.")
