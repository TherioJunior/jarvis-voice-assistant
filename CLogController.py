import logging
import os
import sys

def Suppress():
    sys.stderr = open(os.devnull, 'w')
    logging.basicConfig(level=logging.ERROR)

# Function to restore STDERR
def Restore():
    sys.stderr = sys.__stderr__
