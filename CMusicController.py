import CSpeechModule
import CInputController


def SimulatePlayPause():
    try:
        CInputController.SimulateKeyPress("XF86AudioPlay")
        CSpeechModule.SpeakWithVolume("Play/Pause command sent.", 25)
        print("Play/Pause command sent.")
    except Exception as e:
        CSpeechModule.SpeakWithVolume(f"Error sending Play/Pause command: {e}", 25)
        print(f"Error sending Play/Pause command: {e}")
