import subprocess


def SpeakWithVolume(text: str = "Undefined", volume: int = 25):
    try:
        subprocess.run(["espeak", f"-a {volume}", text])
    except Exception as e:
        print(f"Error with espeak: {e}")
