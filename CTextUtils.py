import string


def ExtractNumberFromText(text: str = "Undefined"):
    number_map = {
        "one": "1",
        "home": "1",
        "two": "2",
        "too": "2",
        "to": "2",
        "browsing": "2",
        "three": "3",
        "dev": "3",
        "four": "4",
        "for": "4",
        "files": "4",
        "five": "5",
        "settings": "5",
        "six": "6",
        "chats": "6",
        "seven": "7",
        "music": "7",
        "eight": "8",
        "video": "8",
        "nine": "9",
        "network": "9",
        "ten": "10",
        "vm": "10"
    }

    # Split the text into words
    words = text.split()

    # Replace number words with their numeric representations
    replaced_words = [number_map.get(word.lower(), word) for word in words]

    # Reconstruct the string and return
    return ' '.join(replaced_words)


def CleanText(text: str = "Undefined"):
    # Removes punctuation from the text
    return text.translate(str.maketrans('', '', string.punctuation))


def FormatVMName(vm_name: str = "Undefined"):
    vm_name = vm_name.replace("linux coding vm", "silverblue38")
    vm_name = vm_name.replace("coding vm", "win11")
    vm_name = vm_name.replace("school vm", "win11-school")

    vm_name = vm_name.replace(" ", "")
    vm_name = vm_name.replace("minus", "-")
    vm_name = vm_name.replace("dash", "-")

    return vm_name


def GetOrdinalSuffix(day: int = -1):
    if 4 <= day <= 20 or 24 <= day <= 30:
        return "th"
    else:
        return ["st", "nd", "rd"][day % 10 - 1]
