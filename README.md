# JARVIS Voice Assistant

## Setup

Setup a new virtual environment:

```shell
virtualenv venv
```

Activate and enter it:

```shell
source venv/bin/activate
```

Install required packages:

```shell
pip install -r requirements.txt
```

Run the assistant:

```shell
python main.py
```



## Credits

[OpenAI](https://openai.com) for creating their speech-to-text model [Whisper](https://github.com/openai/whisper).
