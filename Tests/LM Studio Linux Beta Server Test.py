# Example: reuse your existing OpenAI setup
import os
import openai

openai.api_base = "http://localhost:1234/v1" # point to the local server
openai.api_key = "" # no need for an API key

completion = openai.ChatCompletion.create(
    model="local-model", # this field is currently unused
    messages=[
        {"role": "system", "content": "Always answer the user directly."},
        {"role": "user", "content": "What's 50/2?."}
    ]
)

print(completion.choices[0].message.content)
