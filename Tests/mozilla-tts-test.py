import subprocess
import simpleaudio as sa
import numpy as np
import wave
import os

def adjust_volume(wav_data, volume):
    """ Adjust the volume of audio data """
    audio_data = np.frombuffer(wav_data, dtype=np.int16)
    max_volume = np.iinfo(np.int16).max
    new_audio_data = (audio_data * volume).astype(np.int16)
    return new_audio_data.tobytes()

def synthesize_and_play(text, out_path, volume=1.0):
    # Generate the WAV file using tts command
    subprocess.run(['tts', '--text', text, '--out_path', out_path])

    # Load the WAV file
    with wave.open(out_path, 'rb') as wf:
        num_channels = wf.getnchannels()
        sample_width = wf.getsampwidth()
        frame_rate = wf.getframerate()
        num_frames = wf.getnframes()
        wav_data = wf.readframes(num_frames)

    # Adjust volume
    adjusted_data = adjust_volume(wav_data, volume)

    # Play the adjusted audio
    wave_obj = sa.WaveObject(adjusted_data, num_channels, sample_width, frame_rate)
    play_obj = wave_obj.play()
    play_obj.wait_done()

    # Delete the WAV file
    os.remove(out_path)

# Example usage: volume is a float, 1.0 is original, 0.5 is half, etc.
synthesize_and_play("This is my text.", "./output.wav", volume=0.25)
