# Define the output zip file name
$outputZip = "archive.zip"

# Remove previous zip file if exists
if (Test-Path $outputZip) {
    Remove-Item $outputZip
}

# List of default files/folders to exclude
$excludeList = @(".git\", ".gitattributes", "README.md", "venv\", ".idea\", "*.wav", "bundle.*", ".gitignore")

# Read .gitignore and update the exclude list
if (Test-Path .\.gitignore) {
    $gitignore = Get-Content .\.gitignore
    $excludeList += $gitignore
}

# Function to test if a file should be excluded based on the exclude list
function ShouldExclude($file, $excludeList) {
    foreach ($exclude in $excludeList) {
        if ($file -like $exclude -or $file -like "./$exclude") {
            return $true
        }
    }
    return $false
}

# Compress files, excluding specified ones
Get-ChildItem -Recurse -File | Where-Object { -not (ShouldExclude $_.FullName $excludeList) } | Compress-Archive -DestinationPath $outputZip -Update

Write-Host "Archive created: $outputZip"

