#!/bin/bash

# Define the output zip file name
output_zip="archive.zip"

# Start with a clean zip file
rm -f "$output_zip"

# Array of patterns to exclude from zipping
exclude_patterns=('*/.git/*' '*/.gitattributes' '*/README.md' '*/venv/*' '*/.idea/*' './*.wav' './bundle.*' './.gitignore')

# Build find exclude arguments
exclude_args=()
for pattern in "${exclude_patterns[@]}"; do
    exclude_args+=(! -path "$pattern")
done

# Function to add files to the zip, excluding the specified patterns
add_to_zip() {
    if [ -f .gitignore ]; then
        # If .gitignore exists, use grep to filter out those files
        find . -type f "${exclude_args[@]}" -print0 | grep -zvxFf .gitignore | xargs -0 zip -ru "$output_zip"
    else
        # If .gitignore does not exist, add all files directly
        find . -type f "${exclude_args[@]}" -print0 | xargs -0 zip -ru "$output_zip"
    fi
}

# Run the function to add files to zip
add_to_zip

echo "Archive created: $output_zip"

